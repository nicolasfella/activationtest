#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <QDesktopServices>
#include <QUrl>

#include <KNotification>

#include <QWindow>
#include <private/qwaylandwindow_p.h>
#include <qnativeinterface.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->pushButton, &QPushButton::clicked, this, [] {
        QDesktopServices::openUrl(QUrl("kde.org"));
    });

    connect(ui->notificationButton, &QPushButton::clicked, this, [this] {
        KNotification *noti = new KNotification("notification");
        noti->setComponentName("plasma_workspace");
        noti->setText("Hello World");
        noti->setTitle("Hi");
        noti->setDefaultAction("Open");

        connect(noti, &KNotification::defaultActivated, this, [this, noti] {
            auto waylandWindow = windowHandle()->nativeInterface<QNativeInterface::Private::QWaylandWindow>();
            waylandWindow->setXdgActivationToken(noti->xdgActivationToken());
            activateWindow();
        });

        noti->sendEvent();
    });
}

MainWindow::~MainWindow()
{
    delete ui;
}
